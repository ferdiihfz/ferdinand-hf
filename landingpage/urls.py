from django.urls import path
from . import views

app_name = 'landingpage'

urlpatterns = [
    path('', views.landingpage, name='landingpage'),
    path('profile/', views.profile, name='profile'),
    path('about/', views.about, name='about'),
    path('portfolio/', views.portfolio, name='portfolio'),
    path('gallery/', views.gallery, name='gallery'),
    path('agenda/', views.agenda, name='agenda'),
    path('agenda/new', views.agenda_new, name='agenda_new'),
    path('agenda/delete', views.agenda_delete, name='agenda_delete'),
]
