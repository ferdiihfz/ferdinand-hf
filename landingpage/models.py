from django.db import models
from datetime import datetime

# Create your models here.


class Agenda(models.Model):

    activity = models.CharField(max_length=96)
    day = models.DateTimeField(default=datetime.now)
    time = models.TimeField()
    place = models.CharField(max_length=32)
    category = models.CharField(max_length=32)

    def __str__(self):
        return self.activity