from django.shortcuts import render
from .models import Agenda
from . import forms

# Create your views here.

def landingpage(request):
    return render(request, 'story3_landing.html')

def about(request):
    return render(request, 'story3_about.html')

def profile(request):
    return render(request, 'story3_profile.html')

def gallery(request):
    return render(request, 'story3_gallery.html')

def portfolio(request):
    return render(request, 'story3_portofolio.html')

def agenda(request):
    new_agenda = Agenda.objects.all().order_by('day')
    return render(request, 'story3_agenda.html', {'agenda': new_agenda})

def agenda_new(request):
    form = Schedule_Form(request.POST or None)
    if request.method == 'POST' and 'submit' in request.POST:
        form = forms.AgendaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('agenda')

    else:
        form = forms.AgendaForm()
    return render(request, 'story3_agendacreate.html', {'form': form})

def agenda_delete(request):
	Agenda.objects.all().delete()
	return render(request, "story3_agenda.html")



