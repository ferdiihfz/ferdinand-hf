from django import forms
from .models import Agenda

class DateTimeInput(forms.DateTimeInput):
    input_type = 'datetime-local'

class AgendaForm(forms.ModelForm):
    class Meta:
        model = Agenda
        fields = ['activity', 'day', 'time', 'place', 'category']
        widgets = {
            'day': forms.DateInput(attrs={'type': 'day'}),
            'time': forms.TimeInput(attrs={'type': 'time'})}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })